<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserResource;

class WhoisController extends Controller
{
    public function user()
    {
        if(auth()->check()){
            $user = auth()->user();
            return new UserResource($user);
        }
        return response()->json(null, 401);
    }
}
 