<?php

namespace App\Http\Controllers\Design;

use App\Design;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\DesignResource;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UpdateDesignRequest;

class DesignController extends Controller
{

    public function update(UpdateDesignRequest $request, $id)
    {
        $design = Design::findOrFail($id);
        $this->authorize('update', $design);

        $design->update([
            'title' => $request->title,
            'description' => $request->description,
            'slug' => Str::slug($request->title),
            'is_live' => ! $design->upload_successful ? false : $request->is_live
        ]);
        $design->retag( $request->tags);



        return new DesignResource($design);
        //return response()->json($design, 200);
    }

    public function destroy($id)
    {
        $design = Design::findOrFail($id);
        $this->authorize('delete', $design);

        // delete the files associated to the record
        foreach(['thumbnail', 'large', 'original'] as $size){
            // check if the file exists in the database
            if(Storage::disk($design->disk)->exists("uploads/designs/{$size}/".$design->image)){
                Storage::disk($design->disk)->delete("uploads/designs/{$size}/".$design->image);
            }
        }

        $design->delete();

        return response()->json(['message' => 'Record deleted'], 200);

    }
}
