<?php

namespace App\Http\Controllers\Design;

use App\Design;
use App\Jobs\UploadImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UploadDesignRequest;

class UploadController extends Controller
{
    public function upload(UploadDesignRequest $request)
    {

        $image = $request->file('image');
        $filename = time()."_". str_replace(' ', '_', strtolower($image->getClientOriginalName()));
        // move the image to the temporary location (tmp)
        $tmp = $image->storeAs('uploads/original', $filename, 'tmp');

        $design =Design::create([
            'user_id' =>Auth::id(),
            'image' => $filename,
            'disk' => config('site.upload_disk')
        ]);

        // dispatch a job to handle the image manipulation
         $this->dispatch(new UploadImage($design));

        return response()->json($design, 200);

    }
}
