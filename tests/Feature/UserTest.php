<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;


class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @test
     */
    public function guest_can_register()
    {
       // $this->withoutExceptionHandling();
        $data= [
            'name'=>'Name',
            'email'=>'eamil1@email.com',
            'username'=>'username1',
            'password'=>'password1',
            'cpassword'=>'password1',
        ];
        $response = $this->post('/api/register',$data)
        ->assertStatus(200)
        ->assertJson([
        'name'=>$data['name'],
        'email'=>$data['email'],
        'username'=>$data['username']
          ]);

    }
/** @test */
    public function a_valid_attempt_to_login()
    {
        $user = factory(User::class)->create();
        $data=[
            'email'=>$user->email,
            'password'=>"password"
        ];
        $response = $this->post('/api/login',$data)
        ->assertStatus(200)
        ->dump();

    }
    /** @test */
    public function a_invalid_attempt_to_login()
    {
        $user = factory(User::class)->create();
        $data=[
            'email'=>$user->email,
            'password'=>"password11"
        ];
        $response = $this->postJson('/api/login',$data)
        ->assertSee("The given data was invalid.")
        ->assertSee("Invalid credentials")
        ->assertStatus(422);

    }

    /** @test */
    public function a_user_can_logout_with_valid_token(){
        $user = factory(User::class)->create();
        $data=[
            'email'=>$user->email,
            'password'=>"password"
        ];
        $response = $this->post('/api/login',$data)
        ->assertStatus(200);
        $responseString = json_decode($response->getContent(), true);
        //print  $responseString['token'];

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $responseString['token']
        ])
         ->postJson('/api/logout')
         ->assertSee("Logged out successfully!");

    }
    /** @test */
    public function a_user_can_update_password(){
        $user = factory(User::class)->create();
        $data=[
            'email'=>$user->email,
            'password'=>"password"
        ];
        $response = $this->post('/api/login',$data)
        ->assertStatus(200);
        $responseString = json_decode($response->getContent(), true);


        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $responseString['token']
        ])
         ->putJson('api/settings/password',[
            "current_password"=>"password",
            "password"=>"123456789",
            "cpassword"=>"123456789"
         ])
         ->assertSee("Password updated");
         $responseString = json_decode($response->getContent(), true);
         $this->assertArrayHasKey('message',$responseString);

    }
      /** @test */
    public function a_user_can_upload_image(){
        Storage::fake('tmp');
        $file = UploadedFile::fake()->image('avatar.jpg');
        $user = factory(User::class)->create();
        $data=[
            'email'=>$user->email,
            'password'=>"password"
        ];
        $response = $this->post('/api/login',$data)
        ->assertStatus(200);
        $responseString = json_decode($response->getContent(), true);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $responseString['token']
        ])
         ->postJson('api/designs',[
            'image' => $file,
         ]) ->assertStatus(200);
         $responseString = json_decode($response->getContent(), true);
         $this->assertArrayHasKey('image',$responseString);
         $this->assertArrayHasKey('disk',$responseString);


    }
    /** @test */
    public function a_user_can_update_desgin(){
        Storage::fake('tmp');
        $file = UploadedFile::fake()->image('avatar.jpg');
        $user = factory(User::class)->create();
        $data=[
            'email'=>$user->email,
            'password'=>"password"
        ];
        $response = $this->post('/api/login',$data)
        ->assertStatus(200);
        $responseString = json_decode($response->getContent(), true);
        $token= $responseString['token'];
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' .  $token
        ])
         ->postJson('api/designs',[
            'image' => $file,
         ]) ->assertStatus(200);

         $responseString = json_decode($response->getContent(), true);
         $this->assertArrayHasKey('image',$responseString);
         $this->assertArrayHasKey('disk',$responseString);
         $this->assertArrayHasKey('user_id',$responseString);




         $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' .  $token
        ])
         ->putJson('api/designs/'.$responseString['id'],[
            'title' => "HELLO WORD1a",
            'description' => "description description description",
            "is_live"=>true
         ])
         ->assertStatus(200)
         ->assertSee("HELLO WORD1a")
         ->assertSee("description description description");




    }


}
